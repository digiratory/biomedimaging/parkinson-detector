import os
import sys


if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.dirname(__file__)))
    import parkinson_detector_app.parkinson_detector_app as parkinson_detector_app
    parkinson_detector_app.main()
